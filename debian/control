Source: golang-github-sassoftware-go-rpmutils
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders:
 Simon Josefsson <simon@josefsson.org>,
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-golang,
 golang-any,
 golang-github-datadog-zstd-dev,
 golang-github-stretchr-testify-dev,
 golang-github-ulikunitz-xz-dev,
 golang-github-xi2-xz-dev,
 golang-golang-x-crypto-dev,
 golang-uber-goleak-dev,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-sassoftware-go-rpmutils
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-sassoftware-go-rpmutils.git
Homepage: https://github.com/sassoftware/go-rpmutils
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/sassoftware/go-rpmutils

Package: golang-github-sassoftware-go-rpmutils-dev
Architecture: all
Multi-Arch: foreign
Depends:
 golang-github-datadog-zstd-dev,
 golang-github-stretchr-testify-dev,
 golang-github-ulikunitz-xz-dev,
 golang-github-xi2-xz-dev,
 golang-golang-x-crypto-dev,
 golang-uber-goleak-dev,
 ${misc:Depends},
Description: Golang implementation of parsing RPM packages (library)
 go-rpmutils is a library written in go (http://golang.org) for parsing
 and extracting content from RPMs (http://www.rpm.org).
 .
 go-rpmutils provides a few interfaces for handling RPM packages. There is
 a highlevel Rpm struct that provides access to the RPM header and CPIO
 (https://en.wikipedia.org/wiki/Cpio) payload. The CPIO payload can be
 extracted to a filesystem location via the ExpandPayload function or
 through a Reader interface, similar to the tar implementation
 (https://golang.org/pkg/archive/tar/) in the go standard library.
